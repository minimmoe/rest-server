# thesisrestserver

## Notes

This server listens to two different ports. Another one is pure TLS socket server listener that is used to register information, like loginserver IP, port and so on. Second port is used to query registered data as JSON strings (Like REST).

## Example configuration.json

```json

{
    "General": {
        "IP":"127.0.0.1",
        "RegisterPort":"20101",
        "RegisterTimeoutMS":"200",
        "QueryPort":"20102",
        "Cert": "certs/server.crt",
        "Key": "certs/server.key",
        "Secret":"secret"
    }
}

```

secret is for JWT token so you need to have a passphrase to use the REST part of the server