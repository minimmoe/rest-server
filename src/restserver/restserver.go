package restserver

import (
	"errors"
	"fmt"
	"minimmoe/src/restdata"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

// RESTServer stores data shared by monitor functions
type RESTServer struct {
	Data     restdata.RestData
	Server   http.Server
	IP       string
	Port     string
	Cert     string
	Key      string
	JwtToken []byte
}

// NewRestServer default constructor for REST server
func NewRestServer(IP, Port, CertPath, KeyPath, JWTSecret string) *RESTServer {

	restServer := new(RESTServer)
	restServer.IP = IP
	restServer.Port = Port
	restServer.Cert = CertPath
	restServer.Key = KeyPath
	restServer.JwtToken = []byte(JWTSecret)
	restServer.Data = *restdata.NewRestData()
	restServer.registerRoutes()
	restServer.Server.Addr = restServer.IP + ":" + restServer.Port
	return restServer
}

// Start initializes values and starts REST listener
func (R *RESTServer) Start() error {
	return R.Server.ListenAndServeTLS(R.Cert, R.Key)
}

// Close shutsdown rest server
func (R *RESTServer) Close() {
	R.Server.Shutdown(nil)
}

func (R *RESTServer) isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {

	return http.HandlerFunc(

		func(responseWriter http.ResponseWriter, request *http.Request) {
			// headers alaways start with capital
			if request.Header["Jwttoken"] != nil {

				token, err := jwt.Parse(request.Header["Jwttoken"][0],

					func(token *jwt.Token) (interface{}, error) {

						if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
							return nil, errors.New("There was an error in token.Method")
						}
						return R.JwtToken, nil
					},
				)
				if err != nil {
					fmt.Println(err.Error())
				} else {

					if token.Valid {
						endpoint(responseWriter, request)
					}
				}

			} else {
				fmt.Println("Failed Access Attempt")
				fmt.Fprintf(responseWriter, "{}")
			}
		},
	)
}
