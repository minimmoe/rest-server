package restserver

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func (R *RESTServer) sysMonitorPage(w http.ResponseWriter, r *http.Request) {

	R.loginHandlers(w, r)
	R.connectionHandlers(w, r)
}

func (R *RESTServer) loginHandlers(w http.ResponseWriter, r *http.Request) {

	jsonString, err := R.Data.GetAllLoginHandlers()
	if err != nil {
		fmt.Fprintf(w, "Something went wrong")
		fmt.Println(err.Error())
	}
	fmt.Fprintf(w, string(jsonString))
}

func (R *RESTServer) connectionHandlers(w http.ResponseWriter, r *http.Request) {

	jsonString, err := R.Data.GetAllConnectionHandlers()
	if err != nil {
		fmt.Fprintf(w, "Something went wrong")
		fmt.Println(err.Error())
	}
	fmt.Fprintf(w, string(jsonString))
}

func (R *RESTServer) leastCrowdedConnectionHandler(w http.ResponseWriter, r *http.Request) {

	jsonString, err := R.Data.GetConnectionHandlerWithLowestUsers()
	if err != nil {
		fmt.Fprintf(w, "Something went wrong")
		fmt.Println(err.Error())
	}
	fmt.Fprintf(w, string(jsonString))
}

func (R *RESTServer) checkLoginToken(w http.ResponseWriter, r *http.Request) {

	name := r.Header.Get("Name")
	token := r.Header.Get("Token")

	if err := R.Data.CheckLoginToken(name, token); err != nil {
		fmt.Fprintf(w, "false")
	} else {

		fmt.Fprintf(w, "true")
	}
}

func (R *RESTServer) insertLoginHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		fmt.Fprintf(w, "Something went wrong")
		return
	}

	// HTML Headers always start with capital letter
	name := r.Header.Get("Name")
	ip := r.Header.Get("Ip")
	port := r.Header.Get("Port")
	expiration := 10 * time.Second

	R.Data.NewLoginHandler(name, ip, port, expiration)
}
func (R *RESTServer) insertConnectionHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		fmt.Fprintf(w, "Something went wrong")
		return
	}

	// HTML Headers always start with capital letter
	name := r.Header.Get("Name")
	ip := r.Header.Get("Ip")
	port := r.Header.Get("Port")
	userCount, err := strconv.Atoi(r.Header.Get("Usercount"))
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	expiration := 10 * time.Second

	R.Data.NewConnectionHandler(name, ip, port, strconv.Itoa(userCount), expiration)
}

func (R *RESTServer) insertLoginToken(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		fmt.Fprintf(w, "Something went wrong")
		return
	}

	// HTML Headers always start with capital letter
	userName := r.Header.Get("Username")
	token := r.Header.Get("Token")
	expiration := 1 * time.Second

	R.Data.NewLoginToken(userName, token, expiration)
}

func (R *RESTServer) whatsmyip(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, r.RemoteAddr)
}
