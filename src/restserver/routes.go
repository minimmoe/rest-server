package restserver

import (
	"net/http"
)

func (R *RESTServer) registerRoutes() {

	http.Handle("/sysmonitor", R.isAuthorized(R.sysMonitorPage))

	http.Handle("/loginhandlers", R.isAuthorized(R.loginHandlers))
	http.Handle("/connectionhandlers", R.isAuthorized(R.connectionHandlers))
	http.Handle("/leastcrowdedconnectionhandler", R.isAuthorized(R.leastCrowdedConnectionHandler))
	http.Handle("/checklogintoken", R.isAuthorized(R.checkLoginToken))

	http.Handle("/insertloginhandler", R.isAuthorized(R.insertLoginHandler))
	http.Handle("/insertconnectionhandler", R.isAuthorized(R.insertConnectionHandler))
	http.Handle("/insertlogintoken", R.isAuthorized(R.insertLoginToken))

	http.Handle("/whatsmyip", R.isAuthorized(R.whatsmyip))
}
