package restdata

import (
	"encoding/json"
	"errors"
	"strconv"
	"sync"
	"time"
)

// LoginHandler struct holds all data gathered from a single Login Handler.
type LoginHandler struct {
	Name       string
	IP         string
	Port       string
	Expiration time.Time
}

// ConnectionHandler struct holds all data gathered from a single Connection Handler.
type ConnectionHandler struct {
	Name       string
	IP         string
	Port       string
	UserCount  string
	Expiration time.Time
}

// LoginToken type holds data for a single authorized ConnectionHandler connection.
type LoginToken struct {
	Username   string
	Token      string
	Expiration time.Time
}

// RestData holds all data that is stored and served from REST server.
type RestData struct {
	ConnectionHandlers     map[string]ConnectionHandler
	LoginHandlers          map[string]LoginHandler
	LoginTokens            map[string]LoginToken
	ConnectionHandlersLock sync.Mutex
	LoginHandlersLock      sync.Mutex
	LoginTokensLock        sync.Mutex
}

// NewRestData is a default constructor for RestData type.
func NewRestData() *RestData {
	rd := new(RestData)
	rd.ConnectionHandlers = make(map[string]ConnectionHandler)
	rd.LoginHandlers = make(map[string]LoginHandler)
	rd.LoginTokens = make(map[string]LoginToken)
	return rd
}

// NewLoginHandler Inserts a new login handler information to the RestData map.
// If LoginHandler with same name exists, updates the existing loginhandler information.
func (RD *RestData) NewLoginHandler(name, ip, port string, expiration time.Duration) {
	var lh LoginHandler
	RD.LoginHandlersLock.Lock()
	if val, ok := RD.LoginHandlers[name]; ok {
		lh = val
	} else {
		lh = *new(LoginHandler)
	}
	lh.Name = name
	lh.IP = ip
	lh.Port = port
	lh.Expiration = time.Now().Local().Add(expiration)
	RD.LoginHandlers[name] = lh
	RD.LoginHandlersLock.Unlock()
}

// NewConnectionHandler Inserts a new connection handler information to the RestData map.
// If LoginHandler with same name exists, updates the existing loginhandler information.
func (RD *RestData) NewConnectionHandler(name, ip, port string, userCount string, expiration time.Duration) {
	var ch ConnectionHandler
	RD.ConnectionHandlersLock.Lock()
	if val, ok := RD.ConnectionHandlers[name]; ok {
		ch = val
	} else {
		ch = *new(ConnectionHandler)
	}
	ch.Name = name
	ch.IP = ip
	ch.Port = port
	ch.UserCount = userCount
	ch.Expiration = time.Now().Local().Add(expiration)
	RD.ConnectionHandlers[name] = ch
	RD.ConnectionHandlersLock.Unlock()
}

// NewLoginToken Inserts a new login token information to the RestData map.
func (RD *RestData) NewLoginToken(name, token string, expiration time.Duration) {
	var lt LoginToken
	RD.LoginTokensLock.Lock()
	if val, ok := RD.LoginTokens[name]; ok {
		lt = val
	} else {
		lt = *new(LoginToken)
	}
	lt.Username = name
	lt.Token = token
	lt.Expiration = time.Now().Local().Add(expiration)
	RD.LoginTokens[name] = lt
	RD.LoginTokensLock.Unlock()
}

// DeleteExpiredValues checks expired data from the RestData once and waits for
// t duration. (Best called inside a for loop inside a goroutine)
func (RD *RestData) DeleteExpiredValues(t time.Duration) {

	RD.expiredConnectionHandlers()
	RD.expiredLoginHandlers()
	RD.expiredLoginTokens()
	time.Sleep(t)
}

// 8 53 58
// 8 54 04

func (RD *RestData) expiredConnectionHandlers() {
	RD.ConnectionHandlersLock.Lock()
	for key, value := range RD.ConnectionHandlers {
		if value.Expiration.Hour() <= time.Now().Local().Hour() &&
			value.Expiration.Minute() <= time.Now().Local().Minute() &&
			value.Expiration.Second() < time.Now().Local().Second() {

			delete(RD.ConnectionHandlers, key)
		}
	}
	RD.ConnectionHandlersLock.Unlock()
}

func (RD *RestData) expiredLoginHandlers() {
	RD.LoginHandlersLock.Lock()
	for key, value := range RD.LoginHandlers {
		if value.Expiration.Hour() <= time.Now().Local().Hour() &&
			value.Expiration.Minute() <= time.Now().Local().Minute() &&
			value.Expiration.Second() < time.Now().Local().Second() {

			delete(RD.LoginHandlers, key)
		}
	}
	RD.LoginHandlersLock.Unlock()
}

func (RD *RestData) expiredLoginTokens() {
	RD.LoginTokensLock.Lock()
	for key, value := range RD.LoginTokens {
		if value.Expiration.Hour() <= time.Now().Local().Hour() &&
			value.Expiration.Minute() <= time.Now().Local().Minute() &&
			value.Expiration.Second() < time.Now().Local().Second() {

			delete(RD.LoginTokens, key)
		}
	}
	RD.LoginTokensLock.Unlock()
}

// GetAllLoginHandlers returns all login handlers in the map as json or an error.
func (RD *RestData) GetAllLoginHandlers() (string, error) {
	RD.LoginHandlersLock.Lock()
	jsonString, err := json.Marshal(RD.LoginHandlers)
	if err != nil {
		RD.LoginHandlersLock.Unlock()
		return "", err
	}
	RD.LoginHandlersLock.Unlock()
	return string(jsonString), nil
}

// GetConnectionHandlerWithLowestUsers returns a single connection handler with lowest user count as json.
func (RD *RestData) GetConnectionHandlerWithLowestUsers() (string, error) {
	var lowestUsersKey string
	var lowestUsersValue int
	firstSet := false

	RD.ConnectionHandlersLock.Lock()
	for key, value := range RD.ConnectionHandlers {

		if firstSet == false {
			firstSet = true
			lowestUsersValue, _ = strconv.Atoi(value.UserCount)
			lowestUsersKey = key
			continue // move to next loop
		}

		userCount, _ := strconv.Atoi(value.UserCount)
		if userCount < lowestUsersValue {
			lowestUsersKey = key
		}
	}

	jsonString, err := json.Marshal(RD.ConnectionHandlers[lowestUsersKey])
	if err != nil {
		RD.ConnectionHandlersLock.Unlock()
		return "", err
	}
	RD.ConnectionHandlersLock.Unlock()
	return string(jsonString), nil
}

// GetAllConnectionHandlers returns all connection handlers in the map or an error.
func (RD *RestData) GetAllConnectionHandlers() (string, error) {
	RD.ConnectionHandlersLock.Lock()
	jsonString, err := json.Marshal(RD.ConnectionHandlers)
	if err != nil {
		RD.ConnectionHandlersLock.Unlock()
		return "", err
	}
	RD.ConnectionHandlersLock.Unlock()
	return string(jsonString), nil
}

// CheckLoginToken checks RestData for a matching username token set. Returns
// error or nil if token ok!
func (RD *RestData) CheckLoginToken(username, token string) error {

	RD.LoginTokensLock.Lock()
	if val, ok := RD.LoginTokens[username]; ok {
		if val.Token == token {
			// username and token is found!
			delete(RD.LoginTokens, username)
			RD.LoginTokensLock.Unlock()
			return nil
		}
		// invalid token
		RD.LoginTokensLock.Unlock()
		return errors.New("Invalid session token")
	}
	// could not find a token for username
	RD.LoginTokensLock.Unlock()
	return errors.New("No token found")
}
