module minimmoe

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	gitlab.com/minimmoe/confgetter v0.0.0-20190922193137-c2f15b81fd6d // indirect
	gitlab.com/minimmoe/settings v0.0.0-20200123112047-5a3850bb0526
	gitlab.com/minimmoe/tlstcpserver v0.0.0-20200120163521-c9c2b60fbe28 // indirect
)
