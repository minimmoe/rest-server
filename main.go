package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"minimmoe/src/restserver"

	"gitlab.com/minimmoe/settings"
)

// NOTES TO SELF:
// RegisterSocket is TLS server and gets socket packets to place value inside server

func main() {

	isRunning := true
	var WaitGroup sync.WaitGroup

	configuration := settings.NewSettings("configuration.json")

	// check environments for docker values
	if envValue := os.Getenv("PASSWORD"); len(envValue) > 0 {
		configuration.MappedConfigs["restserver"]["password"] = envValue
	}

	restServer := restserver.NewRestServer(
		configuration.MappedConfigs["restserver"]["ip"],
		configuration.MappedConfigs["restserver"]["port"],
		configuration.MappedConfigs["restserver"]["cert"],
		configuration.MappedConfigs["restserver"]["key"],
		configuration.MappedConfigs["restserver"]["password"],
	)

	go func() {
		for isRunning {
			restServer.Data.DeleteExpiredValues(1 * time.Second)
		}
	}()

	// Signal interrupt routine
	go func() {

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		<-sigs
		close(sigs)
		fmt.Println("Closing...")
		isRunning = false
		WaitGroup.Wait()
		restServer.Close()
		os.Exit(0)
	}()

	fmt.Println("Starting Representational State Transfer services.")
	// func (R *REST) Start(IP, Port, CertPath, KeyPath, JWTSecret string) error {
	if err := restServer.Start(); err != nil {
		fmt.Println(err.Error())
	}
}
